# Ghi chép quá trình làm việc

## Table of contents
1. [Việc build các project nodejs và angular](#buildpr)
- [Chuẩn bị môi trường](#prepare)
- [Các kiến thức cơ bản cần nắm](#basic)
- [Tiến hành build project](#build-pro)
- [Tiến hành quản lý các process bằng pm2](#manage-proc)
2. [Cài đặt và cấu hình reverse proxy IIS trên windows server](#iis)
- [Cài đặt IIS](#install-iis)
- [Cấu hình reverse proxy](#config-iis)
3. [Cài đặt LDAP](#ldap)
4. [Apply CI/CD với bitbucket](#ci/cd)
- [Cài đặt OpenSSH trên windows server](#install-openssh)
- [Thực hiện genarate private key và public key](#gen-key)
- [Cài đặt nhiều ssh key trong một repository](#multissh)
- [Tạo một powershell script để chạy lệnh trên windows server](#ps1)
- [Setup bitbucket pipeline](#pipeline)
5. [Lưu ý](#note)


## Việc build các project nodejs và angular <a id="buildpr"></a>

### Chuẩn bị môi trường  <a id="prepare"></a>

- Cài đặt nodejs, yarn, pm2

### Các kiến thức cơ bản cần nắm <a id="basic"></a>

- **Gulp:**
  - Gulp là task runner được xây dựng trên nền Node JS, giúp chúng ta tự động hoá các thao tác thường diễn ra trong quá trình phát triên website: Minify, kiểm tra lỗi Javascript, compile, build…
  - Làm việc với gulp có thể tham khảo: [reference](https://gulpjs.com/docs/en/getting-started/quick-start).

- **PM2:**
  - Là một trình quản lý process dành cho các ứng dụng nodejs được viết bằng chính nodejs và shell. Sử dụng pm2 để giữ cho các process server còn sống va reload, restart với zero downtime.
  - Lý do sử dụng pm2: 
    - Giữ cho server luôn live và ổn định.
    - Thao tác thông qua cli khá đơn giản và dễ hiểu
    - Tự động start các process nêu như system có reboot lại
  - Tham khảo về sử dụng pm2: [reference](https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/).

### Tiến hành build project <a id="build-pro"></a>

- Project Nodejs
  - Cài đặt gulp để thực hiện tự động hóa các thao tác build
  - Tạo file gulpfile.js lưu trong thư mục gốc của dự án
  - Thực hiện định nghĩa các task mỗi task sẽ thực hiện một nhiệm vụ nào đó(ví dụ như compile typescript, hay là build trên các môi trường khác nhau) sử dụng lệnh `gulp.task()` để tạo một task.
  - Lưu ý: Trong `gulp.task` thì ta có thể thực hiện các command shell bằng việc sử dụng module *gulp- shell* đối với các tác vụ trên windows ta sử dụng *gulp-exec*
  - Đối với dự án nodejs sử dụng typescript ta build bằng cách compile các file typescript bằng lệnh `tsc` sẽ biên dịch dự án được xác định bởi file tsconfig.json. Và sử dụng lệnh `tslint -c tslint.json -p tsconfig.json` để phân tích code typescript về các khả năng đọc, bảo trì, các lỗi chức năng tham khảo thêm: tsc: [reference](https://www.typescriptlang.org/docs/handbook/compiler-options.html). tslint: [reference](https://palantir.github.io/tslint/usage/cli/).

- Project angular
  - Thực hiện build project bằng lệnh `ng build` thêm flag `--prod` để thực hiện việc build cho production ngoài ra trong project angular có file là angular.json giúp ta có thể thay đổi hay định nghĩa các cấu hình build cho các môi trường khác nhau [reference](https://medium.com/@balramchavan/configure-and-build-angular-application-for-different-environments-7e94a3c0af23).

### Tiến hành quản lý các process bằng pm2 <a id="manage-proc"></a>

- PM2 cho phép chúng ta tùy chỉnh hành vi, các tùy chọn, các biến môi trường và logs thông qua một file process. File này thường có các format là javascript, JSON, YAML.
- Tạo một file là ecosystem-<tên_evironment>.json trong thư mục gốc của dự án ví dụ như:
- ![alt text](images/ecosystem.png)
- Ý nghĩa:
  - *name:* Tên của application
  - *script:* đường dẫn tới script để chạy pm2 start
  - *env:* Các biến môi trường sẽ có trong app
- Có thể tham khảo thêm về ý nghĩa của các thuộc tính trong file tại [reference](https://pm2.keymetrics.io/docs/usage/application-declaration/#cli).
- Sau khi đã cấu hình xong file ecosystem cho từng môi trường ta tiến hành chạy lệnh `pm2 start ecosystem-<tên_environment>.json`
- Sau khi chạy xong gõ lệnh `pm2 ls` để xem danh sách các tiến trình đã chạy hoặc `pm2 log <id_process hoặc app_name>` để xem log của một tiến trình.

## Cài đặt và cấu hình reverse proxy IIS trên windows server <a id="iis"></a>

### Cài đặt IIS. <a id="install-iis"></a>

- Việc cài đặt IIS khá đơn giản trên windows server vì đã có sẵn ta chỉ việc bật feature này lên tham khảo cách cài đặt tại [đây](https://www.ods.vn/tai-lieu/huong-dan-cai-dat-iis-tren-windows-server-2019.html).

### Cấu hình reverse proxy. <a id="config-iis"></a>

- Để cấu hình reverse proxy ta cần cài đặt [URL Rewrite Extension](https://www.iis.net/downloads/microsoft/url-rewrite) và [Application Request Routing extension](https://www.iis.net/downloads/microsoft/application-request-routing).***URL Rewrite*** là một kỹ thuật cho phép ghi lại địa chỉ website (URL) từ dạng này thành một dạng khác (URL rewriting). Rewrite URLs sử dụng để tạo ra một địa chỉ Web ngắn hơn và dễ nhìn hơn đối với trang Web giúp cho người dùng dễ nhớ và cho hiệu quả hơn với các công cụ tìm kiếm. ***Application Request Routing*** là tiện ích cho ta thêm các tính năng mở rộng như: load balancing, rule-based routing và nhiều hơn thế nữa.

- Sau khi đã cài xong các extension của iis ta mở ứng dụng Internet Information Services (IIS) Manager sau đó chọn site mà ta muốn cấu hình reverse proxy ví dụ (ở đây là Default website):
  ![des](images/default-site.png)
- Chọn *URL Rewrite* và chọn *Add Rules* để thêm rule mới tại đây ta chọn reverse proxy
  ![des](images/reverse-proxy.png)
- Tại inbound rules thêm địa chỉ của node hay angular app ví dụng ở đây localhost:8083 là địa chỉ của nodejs app các request sẽ được forward về địa chỉ này.
  ![des](images/add-rule.png)
- Để chỉnh sửa rule tại màn hình chính của url rewrite ta chọn như sau:
  ![des](images/edit-rule.png)
- Khi mà ta chỏ 1 subdomain tới ip của server thì tại đây ta sẽ cấu hình cho khi mà truy cập tới subdomain thì sẽ iis sẽ chuyển hướng các request tới localhost:8083
  ![des](images/edit-rule-conf.png)
- Tham khảo tại [đây](https://forums.iis.net/t/1155754.aspx?Sub+Domain+Rewrite)

## Cài đặt LDAP <a id="ldap"></a>

- Cài đặt ldap trên windows server với ad lds (Active Directory Lightweight Directory Services) có thể tham khảo các bước cài đặt tại [đây](https://techcommunity.microsoft.com/t5/sql-server/step-by-step-guide-to-setup-ldaps-on-windows-server/ba-p/385362)
- Lưu ý: khi tới phần tạo Application Directory Partition ta cần biết hoặc hỏi dự án về cấu trúc thư mục mà dự án muốn xây dựng. Có thể đọc thêm tài liệu của microsoft về cách xây dựng, các user mặc đinh, group trong ad lds tại [đây](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-r2-and-2012/hh831593(v=ws.11)).
  ![des](images/setup-dir.png)
- Để test connection và manage ldap ta sử dụng tool ADSI. Cách sử dụng tại [đây](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-r2-and-2008/cc771975(v=ws.11))
  ![des](images/connect-ldap.png)
- Tiến hành thêm user vào directory sau đó chỉnh sửa các properties của user theo thông tin properties mà dự án cần cần ví dụ như uid.
  ![des](images/user-properties.png)

## Apply CI/CD với bitbucket <a id="ci/cd"></a>

### Cài đặt OpenSSH trên windows server <a id="install-openssh"></a>

- Để có thể remote tới server và thực hiện lệnh để restart lại các process bằng pm2 ta phải cài đặt OpenSSH trên windows server để thực hiện connect từ linux tới (ở đây là docker container chạy bởi bitbucket)

- Quá trình cài đặt thực hiện tại [đây](https://docs.microsoft.com/en-us/powershell/scripting/learn/remoting/ssh-remoting-in-powershell-core?view=powershell-7)

### Thực hiện genarate private key và public key <a id="gen-key"></a>

- Trên linux ta có thể sử dụng lệnh sau để generate private key và public key với mục đích là ssh mà không cần mật khẩu như thường ta chạy `ssh-keygen`
- ![des](images/key-gen.png)
- Copy public key lên vào C:\ProgramData\ssh\admintrators_authorized_keys trên windows server

### Cài đặt nhiều ssh key trong một repository <a id="multissh"></a>
- Ta tiến hành khai báo một biến là ssh key trong repository của dự án trên bitbucket. Biến này là private key được generate từ lệnh ssh-keygen sẽ được mã hóa sang base64 sau đó mã base64 này sẽ được decode lại để sử dụng trong quá trình ssh
- Tham khảo [refer](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/#UseSSHkeysinBitbucketPipelines-multiple_keys)

### Tạo một powershell script để chạy lệnh trên windows server <a id="ps1"></a>
- Mục đích để tạo ra script này là để ssh vào server và chạy script để thực hiện thao tác restart lại process qua pm2 thông qua file ecosystem.
- Script này sẽ được đặt trong thư mục gốc của dự án.
- Tạo một file có extension là ps1 ví dụ *deploy-staging.ps1*
- Dưới đây là ví dụ của một file ps1:
  ![tab](images/example-ps1.png)
- File ps1 trên thực hiện các việc sau:
  - Pull code mới nhất từ develop về
  - Xóa thư mục build, và file ecosystem cũ sau đó sync lại thư mục build từ trên AWS S3 về
  - Chạy lệnh yarn install để cài đặt các package mới nếu có
  - Sau đó chạy lệnh để restart lại process trên pm2

### Setup bitbucket pipeline <a id="pipeline"></a>
- Bitbucket pipeline dùng để thực hiện các nhiệm vụ build, deploy tự động mỗi khi có trigger như một hành động merge vào branch nào đó, hay là có một commit mới.
- Tạo một file `bitbucket-pipelines.yml` đặt trong thư mục gốc của dự án. Dưới đây là ví dụ về một bitbucket-pipeline
  ![alt](images/pipeline.png)
- Mặc định bitbucket sẽ dử dụng docker images làm môi trường build ta có thể sử dụng images custom ta tự tạo hoặc các images có sẵn.
- Ở pipeline trên sẽ thực hiện mỗi khi ta đẩy gì đó lên branch staging. Xem thêm về pipeline trigger tại [đây](https://support.atlassian.com/bitbucket-cloud/docs/pipeline-triggers/)
- Bitbucket có thể giúp ta cache lại những build dependence bên ngoài hay là các thư mục như một thư viện bên thứ 3 giúp ta có thể chạy build nnhanh hơn trong các lần sau, giảm thời gian chạy. Như trên là cache lại thư mục node_modules của dự án nodejs.
- Sau đó section script là nơi ta thực hiện lệnh build và deploy như trong pipeline trên ta sẽ thực hiện build sau đó xóa thư mục build cũ đc đẩy len S3 và copy lại thư mục và file build lên S3 sau đó sẽ decode SSH key và ssh vào windows server để thực hiện script powershell như đã nói trên.

## Lưu ý <a id="note"></a>

- Ta phải tắt `strict mode: no` của open ssh trên windows server. Mặc định tại  `C:\ProgramData\ssh\sshd_config`
